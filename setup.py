import setuptools
from setuptools.config import read_configuration


setuptools.setup(install_requires=read_configuration("setup.cfg")["options"]["install_requires"])
