import numpy as np
from collections import deque
from scipy.special import logsumexp
from scipy.stats import betabinom
from scipy.stats import beta
from scipy.stats import expon
from scipy.stats import rankdata
from scipy.special import xlogy
from scipy.special import xlog1py
from chessboard.globals import logfile
#from globals import logfile
from scipy.special import gammaln
from sklearn.impute import SimpleImputer
from sklearn.cluster import KMeans
import itertools
from . import __err__
#from __init__ import __err__

np.seterr(invalid='ignore')

# without comb term
#def binomial_logpmf(k,n,p):
#    return xlogy(k, p) + xlog1py(n - k, -p)

class Model():
    def __init__(self,x,psi,am,bm,conc,l,k_init,blocksize,minsize,mcar,verbose):
        # Truncate observations to integer values
        x = np.round(x)

        # Representative junction reads
        self.x = x[0,:,:]
        self.x_mask = np.ma.array(self.x,mask=np.isnan(self.x))

        # Other junction reads
        self.xf = x[1,:,:]
        self.xf_mask = np.ma.array(self.xf,mask=np.isnan(self.xf))

        # Total reads
        self.xn = np.nansum(x,axis=0)

        # Missingness matrix
        self.xm = np.isnan(x[0,:,:]).astype(float)
        self.xm[np.where(self.xm == 0)] = np.nan
        self.xnm = (~np.isnan(x[0,:,:])).astype(float)

        # Matrix dimensions
        self.n = self.x.shape[1]
        self.m = self.x.shape[0]

        # Psi mat
        self.psi = psi

        self.verbose = verbose

        # Priors for values
        #bgidx = (mp.T == np.amax(mp, axis=1)).T # Set minority group to signal
        #self.a = #np.ones((self.m,2)) - 0.5 # This doesnt do anything
        #self.b = #np.ones((self.m,2)) - 0.5
        self.a = np.zeros((self.m,2))
        self.b = np.zeros((self.m,2))
        #bgidx = (mp.T == np.amax(mp, axis=1)).T # Set minority group to signal
        #self.a = np.stack([a[~bgidx],a[bgidx]],axis=1)
        #self.b = np.stack([b[~bgidx],b[bgidx]],axis=1)
        #self.a = np.zeros((self.m,2))
        #self.b = np.zeros((self.m,2))
        #self.a[:,0] = 10.0
        #self.a[:,1] = 50.0
        #self.b[:,0] = 50.0
        #self.b[:,1] = 10.0
        #self.a = np.array(([[1.0,5.0],[1.0,5.0]]))
        #self.b = np.array(([[5.0,1.0],[5.0,1.0]]))
        self.mu = np.arange(0.025,1,0.05)

        # make viable a b

        fg_conc = 10#80#10
        bg_conc = 20#80#20
        print(fg_conc)
        print(bg_conc)
        self.afix = np.concatenate(((np.tile(self.mu,(self.m,1)) * fg_conc)[:,np.newaxis,:] , (np.tile(self.mu,(self.m,1)) * bg_conc)[:,np.newaxis,:] ),1)
        #print(self.afix.shape)
        self.bfix = np.concatenate(((np.tile(1-self.mu,(self.m,1)) * fg_conc)[:,np.newaxis,:],(np.tile(1-self.mu,(self.m,1)) * bg_conc)[:,np.newaxis,:]), 1)
        self.priors = np.concatenate((beta.logpdf(np.tile(self.mu, (self.m, 1)),20,60)[:, np.newaxis, :],beta.logpdf(np.tile(self.mu, (self.m, 1)),60,20)[:, np.newaxis, :]), 1)
        #print(self.priors)
        # Priors for missing values
        self.am = am
        self.bm = bm
        #self.am_par_fix = self.am_par.copy()
        #self.bm_par_fix = self.bm_par.copy()

        # Psi Dont need this
        #self.psifg = None
        #self.psibg = None

        # Set background prior
        #self.theta = np.zeros((self.m,2))
        self.k_init = k_init
  
        # Hyperparameters
        self.conc = conc#conc
        self.minsize = minsize
        #self.tuning_a = 1 # what is this?
        #self.tuning_b = 1
        self.err = np.log(__err__)
        self.mcar = mcar

        # Clustering data structures
        # Cluster labels for columns
        self.c = np.zeros(self.n, dtype=int)
        # Cluster labels for rows
        self.r = np.zeros((self.m,self.n), dtype=int)
        # Unused cluster labels
        #self.open_labels_c = deque(reversed(range(1,self.n)))
        self.open_labels_c = deque(reversed(range(self.k_init, self.n)))
        # Active clusters
        #self.active_c = deque([0])
        # Cluster sizes
        self.csize_c = np.zeros((self.n))

        self.initialize()


        # Initialization
        #self.csize_c[0] = self.n
        #self.am_par[:,1] += np.nansum(self.xm,axis=1)
        #self.bm_par[:,1] += self.n - np.nansum(self.xm,axis=1)
        self.active_c = deque(np.arange(self.k_init))
        self.csize_c[0:self.k_init] = np.bincount(self.c)
        #self.csize_c[0] = self.n - int(self.n/2)
        #self.csize_c[1] = int(self.n/2)
        #self.r[:,0] = np.random.binomial(1,0.5,self.m)
        #self.r[:,1] = 1 - self.r[:,0]
        #self.c[np.random.choice(np.arange(self.n),int(self.n/2),replace=False)] = 1
        #self.c[50:100] = 1

        

        ## LL

        self.fgll = None
        self.bgll = None

        # Posterior summaries (not used?)
        #self.rposterior=None
        #self.cposterior=None

        # Other precomputed data structures
        self.max_block = blocksize
        self.perms = [np.array(list(itertools.product([0, 1], repeat=i + 1))) for i in range(self.max_block)]
        self.reg = [np.log(expon.pdf(np.sum(self.perms[i], axis=1), 0, 1 / l))[:, np.newaxis] for i in range(self.max_block)]
        self.lsv_block_size = 10
        self.lsv_blocks = np.array_split(np.arange(self.m), self.lsv_block_size)


    #@profile(precision=4)

    def initialize(self):
        imp = SimpleImputer(missing_values=np.nan, strategy='mean')
        imp_psi = imp.fit_transform(self.psi.T)
        kmeans = KMeans(n_clusters=self.k_init, random_state=0).fit(imp_psi)
        self.c = kmeans.labels_

        self.a[:,0] = 5
        self.b[:,0] = 5
        self.a[:,1] = 0.5
        self.b[:,1] = 0.5

        #self.theta = np.zeros((self.m,2))
        #self.theta[:,0] = 0.6
        #self.theta[:,1] = 0.1


    def samplePsi(self):
        # Sample FG
        post_a_fg = self.x_mask + self.a[:,[0]]
        post_b_fg = self.xf_mask + self.b[:,[0]]
        self.psifg = np.random.beta(post_a_fg,post_b_fg) # Output is not masked.

        # Sammple BG
        post_a_bg = self.x_mask + self.a[:,[1]]
        post_b_bg = self.xf_mask + self.b[:,[1]]
        self.psibg = np.random.beta(post_a_bg,post_b_bg)



    def sampleAlphaBeta(self):
        n_bins = len(self.mu)
        fgprobs = np.zeros((self.m,n_bins))
        bgprobs = np.zeros((self.m,n_bins))

        # Compute LL under each discrete a/b
        for i in range(n_bins):
            fgll = betabinom.logpmf(self.x, self.xn, self.afix[:,[0],i], self.bfix[:,[0],i])
            fgll = np.nansum(fgll * self.r[:,self.c],axis=1)
            fgll += self.priors[:,0,i]
            fgprobs[:,i] = fgll
            bgll = betabinom.logpmf(self.x, self.xn, self.afix[:,[1],i], self.bfix[:,[1],i])
            bgll = np.nansum(bgll * (1-self.r[:,self.c]),axis=1)
            bgll += self.priors[:,1,i]
            bgprobs[:,i] = bgll

        # Sample a/b for each LSV
        fgprobs = np.exp(fgprobs - logsumexp(fgprobs,axis=1)[:,np.newaxis])
        bgprobs = np.exp(bgprobs - logsumexp(bgprobs,axis=1)[:,np.newaxis])
        cfg = fgprobs.cumsum(axis=1)
        cbg = bgprobs.cumsum(axis=1)
        ufg = np.random.rand(len(cfg), 1)
        ubg = np.random.rand(len(cbg), 1)
        choicesfg = (ufg < cfg).argmax(axis=1)
        choicesbg = (ubg < cbg).argmax(axis=1)
        self.a[:,0] = self.afix[:,0,:][np.arange(self.m),choicesfg]
        self.b[:,0] = self.bfix[:,0,:][np.arange(self.m),choicesfg]
        self.a[:,1] = self.afix[:,1,:][np.arange(self.m),choicesbg]
        self.b[:,1] = self.bfix[:,1,:][np.arange(self.m),choicesbg]

    """
    def sampleAlphaBeta2(self):
        # Precompute common terms
        fglab = self.r[:,self.c].astype(float)
        fglab[np.where(fglab == 0)] = np.nan
        bglab = 1 - self.r[:,self.c].astype(float)
        bglab[np.where(bglab == 0)] = np.nan
        sizefg = np.nansum(fglab,axis=1)
        sizebg = np.nansum(bglab,axis=1)
        size = np.column_stack((sizefg,sizebg))
    
        logpsifg = np.log(self.psifg)
        logpsibg = np.log(self.psibg)
        mlogpsifg = np.log(1-self.psifg)
        mlogpsibg = np.log(1-self.psibg)
        logpfg = np.nansum(logpsifg * fglab,axis=1)
        logpbg = np.nansum(logpsibg * bglab, axis=1)
        logp = np.column_stack((logpfg,logpbg))
        mlogpfg = np.nansum(mlogpsifg * fglab,axis=1)
        mlogpbg = np.nansum(mlogpsibg * bglab,axis=1)
        mlogp = np.column_stack((mlogpfg, mlogpbg))
    
         # Sample a
        a_star = self.a * np.exp(self.tuning_a * (np.random.uniform(0,1,(self.m,2)) - 0.5))
        # num
        fg = np.nansum(gammaln((self.x + a_star[:,[0]]) * fglab),axis=1)
        bg = np.nansum(gammaln((self.x + a_star[:,[1]]) * bglab),axis=1)
        top = np.column_stack((fg,bg))
        fg = np.nansum(gammaln(self.xn + (a_star[:,[0]] + self.b[:,[0]]) * fglab),axis=1)
        bg = np.nansum(gammaln(self.xn + (a_star[:,[1]] + self.b[:,[1]]) * bglab),axis=1)
        bot = np.column_stack((fg,bg))
        num = size * (gammaln(a_star + self.b) - gammaln(a_star)) + top - bot
        num += -2.5 * np.log(a_star + self.b)
        num += np.log(a_star)

        # den
        fg = np.nansum(gammaln((self.x + self.a[:,[0]]) * fglab),axis=1)
        bg = np.nansum(gammaln((self.x + self.a[:,[1]]) * bglab),axis=1)
        top = np.column_stack((fg,bg))
        fg = np.nansum(gammaln(self.xn + (self.a[:,[0]] + self.b[:,[0]]) * fglab),axis=1)
        bg = np.nansum(gammaln(self.xn + (self.a[:,[1]] + self.b[:,[1]]) * bglab),axis=1)
        bot = np.column_stack((fg,bg))
        den = size * (gammaln(self.a + self.b) - gammaln(self.a)) + top - bot
        den += -2.5 * np.log(self.a + self.b)
        den += np.log(self.a)

        # accept reject
        acc = (np.log(np.random.uniform(0, 1, (self.m, 2))) <= num - den) & (a_star > 1)
        self.a[acc] = a_star[acc]

        b_star = self.b * np.exp(self.tuning_b * (np.random.uniform(0,1,(self.m,2)) - 0.5))

        fg = np.nansum(gammaln((self.xf + b_star[:, [0]]) * fglab), axis=1)
        bg = np.nansum(gammaln((self.xf + b_star[:, [1]]) * bglab), axis=1)
        top = np.column_stack((fg, bg))
        fg = np.nansum(gammaln(self.xn + (b_star[:, [0]] + self.a[:, [0]]) * fglab), axis=1)
        bg = np.nansum(gammaln(self.xn + (b_star[:, [1]] + self.a[:, [1]]) * bglab), axis=1)
        bot = np.column_stack((fg, bg))
        num = size * (gammaln(b_star + self.a) - gammaln(b_star)) + top - bot
        num += -2.5 * np.log(b_star + self.a)
        num += np.log(b_star)

        # den
        fg = np.nansum(gammaln((self.xf + self.b[:, [0]]) * fglab), axis=1)
        bg = np.nansum(gammaln((self.xf + self.b[:, [1]]) * bglab), axis=1)
        top = np.column_stack((fg, bg))
        fg = np.nansum(gammaln(self.xn + (self.b[:, [0]] + self.a[:, [0]]) * fglab), axis=1)
        bg = np.nansum(gammaln(self.xn + (self.b[:, [1]] + self.a[:, [1]]) * bglab), axis=1)
        bot = np.column_stack((fg, bg))
        den = size * (gammaln(self.a + self.b) - gammaln(self.b)) + top - bot
        den += -2.5 * np.log(self.a + self.b)
        den += np.log(self.b)
        acc = (np.log(np.random.uniform(0, 1, (self.m, 2))) <= num - den) & (b_star > 1)
        self.b[acc] = b_star[acc]

        num = size * (gammaln(a_star + self.b) - gammaln(a_star)) + a_star * logp
        num += -2.5 * np.log(a_star + self.b)
        num += np.log(a_star)
        den = size * (gammaln(self.a + self.b) - gammaln(self.a)) + self.a * logp
        den += -2.5 * np.log(self.a + self.b)
        den += np.log(self.a)
        acc = (np.log(np.random.uniform(0,1,(self.m,2))) <= num - den) & (a_star + self.b > 10)
        self.a[acc] = a_star[acc]
        # Sample b
        b_star = self.b * np.exp(self.tuning_b * (np.random.uniform(0,1,(self.m,2)) - 0.5))
        num = size * (gammaln(self.a + b_star) - gammaln(b_star)) + b_star * mlogp
        num += -2.5 * np.log(self.a + b_star)
        num += np.log(b_star)
        den = size * (gammaln(self.a + self.b) - gammaln(self.b)) + self.b * mlogp
        den += -2.5 * np.log(self.a + self.b)
        den += np.log(self.b)
        acc = (np.log(np.random.uniform(0,1,(self.m,2))) <= num - den) & (b_star + self.a > 10)
        self.b[acc] = b_star[acc]
    """


    def computeLL(self):
        self.fgll = betabinom.logpmf(self.x, self.xn, self.a[:,[0]], self.b[:,[0]])
        self.bgll = betabinom.logpmf(self.x, self.xn, self.a[:,[1]], self.b[:,[1]])
        #self.fgll = binomial_logpmf(self.x,self.xn,self.psifg)
        #self.bgll = binomial_logpmf(self.x,self.xn,self.psibg)
        mask = self.r[:,self.c]
        am_post_fg = self.am[:, 0] + np.nansum(self.xm * mask, axis=1)
        bm_post_fg = self.bm[:, 0] + np.nansum(self.xnm * mask, axis=1)
        am_post_bg = self.am[:, 1] + np.nansum(self.xm * (1 - mask), axis=1)
        bm_post_bg = self.bm[:, 1] + np.nansum(self.xnm * (1 - mask), axis=1)
        missfgll = betabinom.logpmf(self.xm, 1, am_post_fg[:,np.newaxis], bm_post_fg[:,np.newaxis])
        missbgll = betabinom.logpmf(self.xm, 1, am_post_bg[:,np.newaxis], bm_post_bg[:,np.newaxis])

        self.fgll = np.nansum(np.dstack((self.fgll, missfgll)),2)
        self.bgll = np.nansum(np.dstack((self.bgll, missbgll)),2)

    def sampleColClusts(self):
        # Iterate through all samples
        for i in range(self.n):
            # Pop current sample
            self.csize_c[self.c[i]] -= 1
            if self.csize_c[self.c[i]] == 0:
                self.active_c.remove(self.c[i])
                self.open_labels_c.append(self.c[i])

            # Generate vector for opening new cluster
            p = np.exp(self.fgll[:, i] - logsumexp(np.column_stack((self.fgll[:, i], self.bgll[:, i])), axis=1))
            random_r = (np.random.rand(self.m,1)[:,0] < p).astype(int)

            # Compute LL of sample under each cluster
            r1 = np.column_stack((self.r[:,self.active_c],random_r))
            ll = self.fgll[:,[i]] * r1 + self.bgll[:,[i]] * (1-r1)
            ll = np.sum(ll,axis=0)
            ll += np.append(np.log(self.csize_c[self.active_c]) - np.log(self.n - 1 + self.conc), np.log(self.conc) - np.log(self.n - 1 + self.conc))

            # Sample a new cluster
            probs = np.exp(ll - logsumexp(ll))
            update = np.random.choice(np.arange(len(ll)),1,p=probs)[0]

            if update == len(ll) - 1:
                # Open new cluster
                new_c = self.open_labels_c.pop()
                self.r[:,new_c] = random_r
                self.active_c.append(new_c)
                self.c[i] = new_c
                self.csize_c[new_c] = 1
            else:
                new_c = self.active_c[update]
                self.csize_c[new_c] += 1
                self.c[i] = new_c

    def sampleTheta(self):
        # Compute posterior count matrix
        mask = self.r[:,self.c]

        self.am_par[:, 0] = self.am_par_fix[:, 0] + np.nansum(self.xm * mask, axis=1)
        self.bm_par[:, 0] = self.bm_par_fix[:, 0] + np.nansum(self.xnm * mask, axis=1)
        self.am_par[:, 1] = self.am_par_fix[:, 1] + np.nansum(self.xm * (1 - mask), axis=1)
        self.bm_par[:, 1] = self.bm_par_fix[:, 1] + np.nansum(self.xnm * (1 - mask), axis=1)
        self.theta[:, 0] = np.random.beta(self.am_par[:, 0], self.bm_par[:, 0])
        self.theta[:, 1] = np.random.beta(self.am_par[:, 1], self.bm_par[:, 1])
        self.theta[np.where(self.theta <0.05)] = 0.05
        self.theta[np.where(self.theta > 0.95)] = 0.95


    def sampleRowClusts(self):

        # Split active clusters into blocks to process
        active_clust = np.sort(self.active_c)
        blocks = np.array_split(active_clust,np.ceil(len(active_clust) / self.max_block))
        total_blocks = len(blocks)
        logfile.write("Total number of blocks: %s\n" % total_blocks)

        for i, b in enumerate(blocks):
            logfile.write("Processing block %s/%s\n" % (i,total_blocks))
            logfile.flush()
            block_loc = np.where(np.isin(self.c,b))[0]
            c = self.c[block_loc]
            n_c = len(b) - 1
            idx = rankdata(c,method="dense") - 1
            labs = []
            # Process rows in lsv blocks to save memory (rows are independent)
            for j, l in enumerate(self.lsv_blocks):
                fglab = np.repeat(self.perms[n_c][:,np.newaxis,idx], len(l), axis=1)
                bglab = 1 - fglab
                bg = np.repeat(self.bgll[np.newaxis,:,block_loc][:,l,:],self.perms[n_c].shape[0],axis=0)
                fg = np.repeat(self.fgll[np.newaxis,:,block_loc][:,l,:],self.perms[n_c].shape[0],axis=0)
                t1 = bglab * bg
                t2 = fglab * fg
                # Generate comb x lsv_block mat
                llmat = np.nansum(t1 + t2,axis=2) + self.reg[n_c] # Regularization at level of cluster blocks

                normconst = logsumexp(llmat,axis=0)
                probmat = np.exp(llmat - normconst).T
                c = probmat.cumsum(axis=1)
                u = np.random.rand(len(c), 1)
                choices = (u < c).argmax(axis=1)
                labs.append(self.perms[n_c][choices,:])

            # Update all lsvs in block for current cluster block
            self.r[:,b] = np.vstack(labs)

    def shuffle(self):
        #means = self.a / (self.a + self.b)
        #idx = np.where(np.abs(means[:,0] - means[:,1]) < 0.2)[0]
        self.a[:,0] = 5
        self.a[:,1] = 0.5
        self.b[:,0] = 5
        self.b[:,1] = 0.5

    def inference(self,iter,burnin,thin):

        total_post_samples = int((iter - burnin) / thin)
        #cposterior = np.zeros((total_post_samples,self.n))
        #rposterior = np.zeros((total_post_samples,self.m,self.n))
        a_posterior = np.zeros((total_post_samples,self.m,2))
        b_posterior = np.zeros((total_post_samples,self.m,2))
        a_post_chain = np.zeros((iter,self.m,2))
        b_post_chain = np.zeros((iter,self.m,2))
        #am_posterior = np.zeros((total_post_samples,self.m,2))
        #bm_posterior = np.zeros((total_post_samples,self.m,2))
        post_samples_pw = np.zeros((total_post_samples,self.n,self.n))
        post_samples_fg = np.zeros((total_post_samples,self.m,self.n))
        med_tiles = np.zeros(total_post_samples)
        cursample = 0
        for i in range(iter):
            if self.verbose:
                print("Iteration: %s" % i)
                #print(self.c)
            logfile.write("Starting Iteration %s\n" % i)
            logfile.flush()
            self.computeLL()
            self.sampleRowClusts()
            self.sampleColClusts()
            self.sampleAlphaBeta()
            #if i % 5 == 0:
            #    self.shuffle()
            #self.sampleTheta()
            a_post_chain[i,:,:] = self.a
            b_post_chain[i,:,:] = self.b


            if i >= burnin and (i + 1 - burnin) % thin == 0:
                med_tiles[cursample] = np.sum(np.bincount(self.c) > self.minsize)
                logfile.write("Total tiles: %s\n" % med_tiles[cursample])
                #cposterior[cursample,:] = self.c
                #rposterior[cursample,:,:] = self.r
                a_posterior[cursample,:,:] = self.a
                b_posterior[cursample,:,:] = self.b
                #am_posterior[cursample,:,:] = self.am_par
                #bm_posterior[cursample,:,:] = self.bm_par
                tmp = np.tile(self.c, (self.n, 1))
                post_samples_pw[cursample,:,:] = tmp.T == tmp
                post_samples_fg[cursample,:,:] = self.r[:,self.c]
                cursample += 1

        med_tiles = int(np.median(med_tiles))
        post_samples_pw_mean = np.sum(post_samples_pw, axis=0) / total_post_samples
        post_samples_fg_mean = np.sum(post_samples_fg, axis=0) / total_post_samples

        lsq = np.zeros(total_post_samples)
        for i in range(total_post_samples):
            lsq = np.sum((post_samples_pw[i, :, :] - post_samples_pw_mean) ** 2)
        idx_opt = np.argmax(lsq)

        #best_c = cposterior[idx_opt,:]
        #best_r = rposterior[idx_opt,:,:]
        a_posterior_best = a_posterior[idx_opt, :, :]
        b_posterior_best = b_posterior[idx_opt, :, :]

        #am_posterior = am_posterior[idx_opt, :, :]
        #bm_posterior = bm_posterior[idx_opt, :, :]

        return a_posterior_best, b_posterior_best, post_samples_pw_mean, post_samples_fg_mean, med_tiles, a_post_chain, b_post_chain









