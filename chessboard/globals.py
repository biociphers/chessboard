import os

from . import __version__
from chessboard.arg_parser import *
#from arg_parser import *
#from __init__ import __version__

args = get_args(__version__)
d = get_consolidate_arguments_and_vars(args)

if not os.path.exists(d["out_dir"]):
    os.mkdir(d["out_dir"])

logfile = open(d["logfile"], "w")
