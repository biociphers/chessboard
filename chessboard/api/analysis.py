import numpy as np
import pandas as pd
import re
from scipy.stats import fisher_exact
from scipy.cluster.hierarchy import linkage, dendrogram
from scipy.spatial.distance import pdist
from scipy.stats import betabinom
from .io import Data

def outputLSVLists(data:Data, file:str):
    """
    Output Excel ``.xlsx`` file containing summary statistics

    Outputs ``.xlsx`` file with the following tabs:
    1. For each discovered tile, a tab showing the samples and LSVs in the tile along with the marignal probability of
    sample and LSV assignment to the tile.
    2. A tab containing all LSVs that did not appear in any cluster (consensus background).
    3. A tab showing missing value signals. For each LSV, if the signal group had an excess of missing values, show
    the increased missingness rate and fisher p-value of missingness enrichment.

    Parameters
    ----------
    index
        CHESSBOARD object containing the data. CHESSBOARD output must be added and summarized. See :func:`chessboard.api.io`
        and :func:`chessboard.api.postsum`
    file
        Path to output file.
    """
    writer = pd.ExcelWriter(file, engine='xlsxwriter')
    #mg = mygene.MyGeneInfo()

    # Write tile info
    for i in np.unique(data.c):

        samples = data.samples[data.c == i]
        dat = pd.DataFrame({'Samples': samples.astype('U50')})
        dat.to_excel(writer, sheet_name='Samples ' + str(i))

        fgidx = data.r[:,data.c == i][:,0] == 1
        lsvs = data.lsvs[fgidx].astype(str)
        #genes = [re.findall(r"gene:(.*?):.*", x.decode('UTF-8'))[0] for x in lsvs]
        genes = [re.findall(r"gene:(.*?):.*", x)[0] for x in lsvs]
        #tmp = mg.querymany(genes, scopes='ensembl.gene', fields='symbol', species='human',returnall=False)
        #for i,x in enumerate(genes):
        #    if x != tmp[i]["query"]:
        #        print(x)
        #genes2 = [x["symbol"] for x in mg.querymany(genes, scopes='ensembl.gene', fields='symbol', species='human',returnall=False)]
        avgfg = np.mean(data.prob_fg[np.ix_(fgidx, data.c == i)], axis=1)
        dat = pd.DataFrame({'LSV':lsvs, 'Gene': genes, "Average Probability Signal":avgfg})
        dat.to_excel(writer, sheet_name='Cluster ' + str(i))

        bgidx = data.r[:,data.c == i][:,0] == 0
        lsvs = data.lsvs[bgidx].astype(str)
        #genes = [re.findall(r"gene:(.*?):.*", x.decode('UTF-8'))[0] for x in lsvs]
        genes = [re.findall(r"gene:(.*?):.*", x)[0] for x in lsvs]
        #genes = [x["symbol"] for x in mg.querymany(genes, scopes='ensembl.gene', fields='symbol', species='human',returnall=False)]
        avgfg = np.mean(data.prob_fg[np.ix_(bgidx, data.c == i)], axis=1)
        dat = pd.DataFrame({'LSV':lsvs, 'Gene': genes, "Average Probability Signal":avgfg})
        dat.to_excel(writer, sheet_name='Background ' + str(i))


    bgidx = np.sum(data.r,axis=1) == 0
    lsvs = data.lsvs[bgidx].astype(str)
    #genes = [re.findall(r"gene:(.*?):.*", x.decode('UTF-8'))[0] for x in lsvs]
    genes = [re.findall(r"gene:(.*?):.*", x)[0] for x in lsvs]
    #genes = [x["symbol"] for x in
    #mg.querymany(genes, scopes='ensembl.gene', fields='symbol', species='human', returnall=False)]
    avgfg = np.mean(data.prob_fg[np.ix_(bgidx, data.c == i)], axis=1)
    dat = pd.DataFrame({'LSV': lsvs, 'Gene': genes, "Average Probability Signal":avgfg})
    dat.to_excel(writer, sheet_name='Consensus Background')

    missmat = np.isnan(data.psi).astype(int)
    m = data.psi.shape[0]
    n = data.psi.shape[1]
    nullmiss = np.sum(missmat,axis=1)
    fg_miss = np.sum(missmat * (data.r == 1).astype(int),axis=1)
    total_sig = np.sum(data.r,axis=1)
    pval = np.zeros(m)
    nullmissrate = nullmiss/n
    with np.errstate(invalid='ignore'):
        signalmissrate = fg_miss / total_sig
    for i in range(m):
        pval[i] = fisher_exact([[total_sig[i] - fg_miss[i],n - nullmiss[i]],[fg_miss[i],nullmiss[i]]])[1]
    pval[np.sum(data.r,axis=1) == 0] = np.nan

    colorder = np.argsort(data.c)
    roworder = np.array(dendrogram(linkage(pdist(data.r)))["ivl"], dtype=int)

    """
    dat = data.psi.copy()
    dat = dat[:, colorder]
    dat = dat[roworder, :]
    rowlabs = data.r.copy()
    rowlabs = rowlabs[roworder, :]
    order = np.arange(data.lsvs.shape[0])[roworder]

    for x in np.unique(rowlabs, axis=0):
        idx = []
        for i in range(dat.shape[0]):
            if np.all(rowlabs[i, :] == x):
                idx.append(i)
        tmp = dat[idx, :]
        tmp2 = tmp[:, x == 1]
        tmp3 = order[idx]
        if np.sum(x) > 0:
            sort = np.argsort(np.nanmean(tmp2, axis=1))
            tmp = tmp[sort, :]
            tmp3 = tmp3[sort]
            dat[idx, :] = tmp
            order[idx] = tmp3
    """

    #genes = np.array([re.findall(r"gene:(.*?):.*", x.decode('UTF-8'))[0] for x in data.lsvs])
    genes = np.array([re.findall(r"gene:(.*?):.*", x)[0] for x in data.lsvs.astype(str)])
    #genes = [x["symbol"] for x in mg.querymany(genes, scopes='ensembl.gene', fields='symbol', species='human', returnall=False)]
    #dat = pd.DataFrame({'LSV': data.lsvs.astype('U50')[order], 'Gene': genes[order],
    #                    'Null Missing Rate': nullmissrate[order], 'Signal Missing Rate': signalmissrate[order], "Delta Miss Rate": (np.abs(nullmissrate - signalmissrate))[order],
    #                    'p-value': pval[order]})
    dat = pd.DataFrame({'LSV': data.lsvs.astype('U50'), 'Gene': genes,
                        'Null Missing Rate': nullmissrate, 'Signal Missing Rate': signalmissrate, "Delta Miss Rate": (np.abs(nullmissrate - signalmissrate)),
                        'p-value': pval})
    dat.to_excel(writer, sheet_name='Probability Missing Signal')






    writer.save()

""""
def computePosteriorModel(data):

    bgidx = (data.mp.T == np.amax(data.mp, axis=1)).T  # Set minority group to signal
    a = np.stack([data.a[~bgidx], data.a[bgidx]], axis=1)
    b = np.stack([data.b[~bgidx], data.b[bgidx]], axis=1)
    am = data.am.copy()
    bm = data.bm.copy()
    a[:,0] = a[:,0] + np.nansum(data.reads[0,:,:] * data.r,axis=1)
    a[:,1] = a[:,1] + np.nansum(data.reads[0,:,:] * (1-data.r),axis=1)
    b[:,0] = b[:,0] + np.nansum(data.reads[1,:,:] * data.r,axis=1)
    b[:,1] = b[:,1] + np.nansum(data.reads[1,:,:] * (1-data.r),axis=1)
    missmat = np.isnan(data.psi).astype(float)
    am[:, 0] = am[:, 0] + np.nansum(missmat * data.r, axis=1)
    am[:, 1] = am[:, 1] + np.nansum(missmat * (1 - data.r), axis=1)
    bm[:, 0] = bm[:, 0] + np.nansum(missmat * data.r, axis=1)
    bm[:, 1] = bm[:, 1] + np.nansum(missmat * (1 - data.r), axis=1)
    data.a_post = a
    data.b_post = b
    data.am_post = am
    data.bm_post = bm
"""

def computeLikelihood(data:Data,x:np.ndarray,r:np.ndarray):
    """
    Compute likelihood on held out test data.

    Once the CHESSBOARD model is trained on a dataset and distributional parameters are learned, one can make predictions
    on held out data. This function returns the likelihood of cluster assignments given a sample vector and tile assignment
    vector. The tile vector is a binary vector of the sample length as the LSVs in the sample vector which indicate whether
    each LSV is signal (1) or background (0).

    Parameters
    ----------
    data
        Chessboard object. Must contain CHESSBOARD output after using :func:`chessboard.api.io.Data.addChessboardOutput`.
    x
        A vector containing the junction spanning read counts of a sample. The first axis is the junction read counts (index 0)
        and alternate junction read counts (index 1). The second axis is LSVs. The total number of LSVs must be sample
        as the data used to train CHESSBOARD.
    r
        Binary vector that indicates whether each sample in the LSV is signal (1) or background (0).

    Returns
    -------
    ``ndarray``
        Vector of log likelihoods where each entry represents assignment to one of the k clusters.

    """
    np.seterr(divide='ignore')
    err = np.log(10e-5)
    a = data.a_post[:, 0] * r + data.a_post[:, 1] * (1 - r)
    b = data.b_post[:, 0] * r + data.b_post[:, 1] * (1 - r)
    am = data.am_post[:, 0] * r + data.am_post[:, 1] * (1 - r)
    bm = data.bm_post[:, 0] * r + data.bm_post[:, 1] * (1 - r)
    xm = np.isnan(x[0, :]).astype(float)
    xm[xm == 0] = np.nan
    xn = np.round(np.sum(x, axis=0))
    #mll = np.nansum(np.log(betabinom.pmf(xm, 1, am, bm)), axis=0)
    mll = np.log(betabinom.pmf(xm, 1, am, bm))
    vll = np.log(betabinom.pmf(np.round(x[0, :]), xn, a, b))
    vll[np.where(vll == -np.inf)[0]] = err
    ll = np.nansum([mll,vll],axis=0)
    return ll

