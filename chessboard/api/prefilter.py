import numpy as np
from scipy.stats import beta
from scipy.stats import kstest
from .util import progressBar
from .io import Data

def VarFilter(data:Data,var_thresh:float):
    """Variance filter

    Filters the LSVs in a Chessboard object by variance up to some variance threshold.

    Parameters
    ----------
    data
        Chessboard object.
    var_thresh
        Variance threshold. LSVs with variance across the samples above this value are retained.
     """
    var = np.nanvar(data.psi, axis=1)
    keep = np.where(var > var_thresh)[0]
    data.psi = data.psi[keep, :]
    data.lsvs = data.lsvs[keep]
    data.reads = data.reads[:,keep,:]


def pbs(x,n_boot=500):
    n = len(x)
    a,b,_,_ = beta.fit(x,floc=0,fscale=1)
    d_obs = kstest(x,beta.cdf,args=(a,b))[0]
    d_boot = np.zeros(n_boot)
    for i in range(n_boot):
        x_sim = np.random.beta(a,b,n)
        x_sim = (x_sim * (n - 1) + 0.5) / n
        d_boot[i] = kstest(x_sim,beta.cdf,args=(a,b))[0]
    p = np.sum(d_obs < d_boot) / n_boot
    return p

def PBSFilter(data,n_boot=500,alpha=0.05):
    """PBS KS Filter

    Filters LSVs using a parametric bootstrap Kolmogorov-Smirnov test.

    Parameters
    ----------
     data
        Chessboard object.
     n_boot
        Number of bootstrap samples.
     alpha
        p-value cutoff for the statistical test.
     """
    print("Beginning Parametric Bootstrap Kolmogorov-Smirnov Test.")
    pval = np.zeros(data.num_lsvs)
    for i in progressBar(range(data.num_lsvs), prefix = 'Progress:', suffix = 'Complete', length = 50):
        lsv_psi = data.psi[i,:][~np.isnan(data.psi[i,:])]
        lsv_psi = (lsv_psi * (len(lsv_psi) - 1) + 0.5) / len(lsv_psi)
        pval[i] = pbs(lsv_psi,n_boot=n_boot)
    reject = np.where(pval < alpha)[0]
    data.pbsfilter = reject
    data.reads = data.reads[:,reject,:]
    data.psi = data.psi[reject,:]
    data.lsvs = data.lsvs[reject]
