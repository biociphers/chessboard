from statsmodels.distributions.empirical_distribution import ECDF
from .io import Data
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
from scipy.spatial import distance
from scipy.cluster import hierarchy
from sklearn.cluster import AgglomerativeClustering
from scipy.cluster.hierarchy import linkage, dendrogram
from scipy.spatial.distance import pdist
import pandas as pd
import seaborn as sns
import scipy as sc
from .io import Data

def PlotECDF(data:Data):
    """ECDF plot

    Plots a empirical CDF (ECDF) of the :math:`\Psi` variances for LSVs across samples. Use this to help determine
    reasonable variance cutoffs for filtering.

    Parameters
    ----------
    data
        Chessboard object.
     """
    var = np.nanvar(data.psi,axis=1)
    ecdf = ECDF(var)
    plt.plot(ecdf.x,ecdf.y)
    plt.xlabel("Variance")
    plt.ylabel("F(X)")


def PlotPointEstimate(data:Data,save:str=None):
    """Heatmap cluster plot

    Plots a heatmap representation of the data and clustering. Note that :math:`\Psi` values are shown in the heatmap and
    white spaces are missing values. CHESSBOARD output should be added to the object.

    Parameters
    ----------
    data
        Chessboard object. Must contain CHESSBOARD output after using :func:`chessboard.api.io.Data.addChessboardOutput`.
    save
        Path to plot output save. If ``None``, the plot is not saved.
    """
    colorder = np.argsort(data.c)
    roworder = np.array(dendrogram(linkage(pdist(data.r)),no_plot=True)["ivl"], dtype=int)

    dat = data.psi.copy()
    dat = dat[:, colorder]
    dat = dat[roworder, :]
    rowlabs = data.r.copy()
    rowlabs = rowlabs[:,colorder]
    rowlabs = rowlabs[roworder, :]

    for x in np.unique(rowlabs, axis=0):
        idx = []
        for i in range(dat.shape[0]):
            if np.all(rowlabs[i, :] == x):
                idx.append(i)
        tmp = dat[idx, :]
        tmp2 = tmp[:, x == 1]
        if np.sum(x) > 0:
            sort = np.argsort(np.nanmean(tmp2, axis=1))
            tmp = tmp[sort, :]
            dat[idx, :] = tmp

    network_labels_col = pd.Index(data.c[colorder].astype(str))
    network_pal_col = sns.color_palette("OrRd", network_labels_col.unique().size)
    network_lut_col = dict(zip(map(str, network_labels_col.unique()), network_pal_col))
    network_colors_col = pd.Series(network_labels_col).map(network_lut_col)
    cmap = sns.cubehelix_palette(as_cmap=True, start=.5, rot=-.75, light=.9)
    cmap.set_bad("white")
    data.vis=dat

    g= sns.clustermap(pd.DataFrame(dat), xticklabels=False, yticklabels=False,
                      row_cluster=False, col_cluster=False, col_colors=network_colors_col,
                      cmap=cmap)

    g.ax_heatmap.set_xlabel("Samples",fontsize=20,labelpad=10)
    g.ax_heatmap.set_ylabel("LSV",fontsize=20,labelpad=20,rotation=270)
    g.savefig(save+"_heatmap.pdf")


    g = sns.clustermap(pd.DataFrame(data.prob_pw[np.ix_(colorder, colorder)]), xticklabels=False, yticklabels=False,
                       row_cluster=False, col_cluster=False, col_colors=network_colors_col,
                       row_colors=network_colors_col, cmap=sns.light_palette("black", as_cmap=True))
    g.ax_heatmap.set_xlabel("Samples",fontsize=20,labelpad=10)
    g.ax_heatmap.set_ylabel("Samples",fontsize=20,labelpad=20,rotation=270)
    g.savefig(save+"_pairwise.pdf")

    g = sns.clustermap(pd.DataFrame(rowlabs), xticklabels=False, yticklabels=False,
                       row_cluster=False, col_cluster=False,cmap=sns.light_palette("black", as_cmap=True))
    g.ax_heatmap.set_xlabel("Samples",fontsize=20,labelpad=10)
    g.ax_heatmap.set_ylabel("LSV",fontsize=20,labelpad=20,rotation=270)
    g.savefig(save+"_labels.pdf")

    g = sns.clustermap(pd.DataFrame(data.prob_fg[np.ix_(roworder, colorder)]), xticklabels=False, yticklabels=False,
                       row_cluster=False, col_cluster=False,cmap=sns.light_palette("black", as_cmap=True))
    g.ax_heatmap.set_xlabel("Samples",fontsize=20,labelpad=10)
    g.ax_heatmap.set_ylabel("LSV",fontsize=20,labelpad=20,rotation=270)
    g.savefig(save+"_marginal.pdf")



def Heatmap(data:Data, row_labels=None, col_labels=None, savefig=None, xlab="Samples", ylab="LSV"):
    """Heatmap plot

    Plots a heatmap representation of the data without clustering. Note that :math:`\Psi` values are shown in the heatmap and
    white spaces are missing values. Custom assignments can be added.

    Parameters
    ----------
    data
        Chessboard object.
    row_labels
        Vector of custom LSV assignments. This should be a binary matrix with n_cluster columns and n_lsv rows. A (1)
        indicates the the LSV is signal in the corresponding cluster.
    col_labels
        Vector of integer labels for samples
    savefig
        Path to plot output save. If ``None``, the plot is not saved.
    xlab
        x-axis label
    ylab
        y-axis label
    """
    colors_row=None
    colors_col=None

    if col_labels is not None:
        labels_col = pd.Index((col_labels).astype(str))
        pal_col = sns.color_palette("OrRd", labels_col.unique().size)
        lut_col = dict(zip(map(str, labels_col.unique()), pal_col))
        colors_col = pd.Series(labels_col).map(lut_col)

    if row_labels is not None:
        labels_row = pd.Index((row_labels).astype(str))
        pal_row = sns.color_palette("OrRd", labels_row.unique().size)
        lut_row = dict(zip(map(str, labels_row.unique()), pal_row))
        colors_row = pd.Series(labels_row).map(lut_row)

    cmap = sns.cubehelix_palette(as_cmap=True, start=.5, rot=-.75, light=.9)
    cmap.set_bad("white")
    g = sns.clustermap(pd.DataFrame(data), xticklabels=False, yticklabels=False,
                       row_cluster=False, col_cluster=False,
                       row_colors=colors_row, col_colors=colors_col,
                       cmap=cmap)

    g.ax_row_dendrogram.set_visible(False)
    g.ax_col_dendrogram.set_visible(False)
    g.ax_heatmap.set_xlabel(xlab, fontsize=20, labelpad=10)
    g.ax_heatmap.set_ylabel(ylab, fontsize=20, labelpad=20, rotation=270)

    if savefig is not None:
        g.savefig(savefig)

def Histogram(data:Data,i:int):
    """LSV Histogram

    Shows a histogram of the indicated LSV with the signal and background separated using a stacked bar plot and the
    posterior distribution curves drawn.

    Parameters
    ----------
    data
        Chessboard object. Must contain CHESSBOARD output after using :func:`chessboard.api.io.Data.addChessboardOutput`.
    i
        Index of the LSV.
    """
    apost = data.a_post[i, :]
    bpost = data.b_post[i, :]

    hdat = data.psi[i, :]
    x = np.linspace(0, 1)
    tmp = []
    for c in np.unique(data.c):
        tmp.append(hdat[data.c == c])
    plt.hist(tmp, bins=20, stacked=True, histtype='bar', color=sns.color_palette("OrRd", len(np.unique(data.c))), density=True)


    rv1 = sc.stats.beta(apost[0], bpost[0])
    rv2 = sc.stats.beta(apost[1], bpost[1])
    plt.plot(x,rv1.pdf(x) * np.sum(data.r[i,:]) / len(data.r[i,:]),color="yellow")
    plt.plot(x, rv2.pdf(x) * (1 - (np.sum(data.r[i, :]) / len(data.r[i, :]))), color="pink")
    plt.ylim(0, 10)

def TracePlot(data:Data,burnin:int,thin:int,save:str=None):
    g=sns.lineplot(data=pd.DataFrame({"log likelihood": data.ll_chain, "iteration": np.arange(len(data.ll_chain))}),
                 x="iteration", y="log likelihood")
    if save is not None:
        g.savefig(save)