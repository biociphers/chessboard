import numpy as np
import scipy as sc
import pandas as pd
from scipy.stats import beta
from scipy.special import logsumexp
from scipy import optimize
from .io import Data
from .util import progressBar
from .io import Data
import warnings

def betaEM(x,quantile=0.5,iter=1000,tol=1e-3):
    k = 2
    n = len(x)
    a = np.zeros(k)
    b = np.zeros(k)
    pi = np.zeros(k)
    # M Step
    w = np.zeros((n,k))
    idx = np.argsort(x)
    w[idx[0:int(n*quantile)],0] = 1
    w[idx[int(n*quantile):n],1] = 1
    prevll = 0
    curll = 0
    ll =None
    converge = np.zeros(iter)
    n_iter = 0
    for t in range(iter):
        n_iter = t
        pi = np.mean(w,axis=0)
        conv = np.zeros(k)
        for i in range(k):
            a[i], b[i], conv[i] = betaMLE(x,w[:,i])
        converge[t] = np.prod(conv)
        # E Step
        tmp = beta.pdf(np.stack((x, x), axis=-1),a,b)
        t1 = np.log(pi,where=pi>0,out=np.full(pi.shape,-np.inf))
        t2 = np.log(tmp,where=tmp>0,out=np.full(tmp.shape,-np.inf))
        ll = t1 + t2
        ll_e = ll
        w = np.exp(ll_e.T - logsumexp(ll_e,axis=1)).T
        curll = np.sum(logsumexp(ll,axis=1))
        if np.abs(curll - prevll) < tol:
            break
        else:
            prevll = curll
            curll = 0
        # M Step
    conv = 1
    if np.prod(converge[int(0.95 * n_iter):n_iter]) == 0:
        conv=0


    return(a,b,pi,prevll,conv)

def _beta_mle_ab(theta, n, s1, s2):
    # Zeros of this function are critical points of
    # the maximum likelihood function.  Solving this system
    # for theta (which contains a and b) gives the MLE for a and b
    # given `n`, `s1` and `s2`.  `s1` is the sum of the logs of the data,
    # and `s2` is the sum of the logs of 1 - data.  `n` is the number
    # of data points.
    a, b = theta
    psiab = sc.special.psi(a + b)
    func = [s1 - n * (-psiab + sc.special.psi(a)),
            s2 - n * (-psiab + sc.special.psi(b))]
    return func


def betaMLE(x, w):
    s1 = np.sum((np.log(x) * w))
    s2 = np.sum(np.log1p(-x) * w)
    ws = np.sum(w)
    conv = 1
    a = 0.5 + (s1 / ws) / (2 * (1 - s2 / ws - s1 / ws))
    b = 0.5 + (s2 / ws) / (2 * (1 - s2 / ws - s1 / ws))

    # Compute the MLE for a and b by solving _beta_mle_ab.
    theta, info, ier, mesg = optimize.fsolve(
        _beta_mle_ab, [a, b],
        args=(ws, s1, s2),
        full_output=True
    )
    a = theta[0]
    b = theta[1]
    v = a * b / ((a + b) ** 2 * (a + b + 1))
    c = np.min([0.0001, a * b / (a + b) ** 2 / 1.01])
    if v < c:
        scale = (a + b) / (a * b / ((a + b) ** 2 * c) - 1)
        a = a / scale
        b = b / scale

    if ier != 1:
        conv = 0

    return a, b, conv

def modelSel(x,q,iter,tol):
    bestll = -np.inf
    bestres = None
    for i in q:
        res = betaEM(x,quantile=i)
        if res[3] > bestll and res[4]:
            bestll = res[3]
            bestres = res
    return bestres


def EstimatePriors(data,quantile=np.arange(0.1, 1, 0.1),iter=1000,tol=1e-3):
    n = data.num_lsvs
    a = np.zeros((n, 2))
    b = np.zeros((n, 2))
    mp = np.zeros((n, 2))
    for i in progressBar(range(n), prefix = 'Progress:', suffix = 'Complete', length = 50):
        lsv_psi = data.psi[i, :][~np.isnan(data.psi[i, :])]
        lsv_psi = (lsv_psi * (len(lsv_psi) - 1) + 0.5) / len(lsv_psi)
        res = modelSel(lsv_psi, quantile, iter, tol)
        if res is not None:
            a[i, :] = res[0]
            b[i, :] = res[1]
            mp[i, :] = res[2]
        else:
            print("Cannot estimate priors for LSV %s" % i)
    data.a = a
    data.b = b
    data.mp = mp

def EstimateMissingPriors(data:Data,background:str,downsample:int=2000):
    r"""\
         Estimate missing value priors.

         Empirically estimate missing value model priors for the signal and background groups. In both cases, the priors
         are estimated using Beta-Binomial regression.

         .. math::
            \upsilon_j \sim BetaBinomial(n, \mu\Phi, (1 − \mu)\Phi)
            logit(\mu) = \beta_0 + \beta_1\chi_j

         Here, :math:`\upsilon_j` is the missingness rate and :math:`\chi_j` is the median read depth for a given LSV
         :math:`j`.

         For the signal, :math:`\upsilon_j` and :math:`\chi_j` are derived from the data. For the background, these quantities
         are obtained from control data. For example, GTEX data for the equivalent tissue. We provide sample data
         for GTEX whole blood in "gtex_missingness.tsv". This file should have a column "miss" representing :math:`\upsilon_j`
         and a column "mdepth" representing :math:`\chi_j`.

         Parameters
         ------
         data
            A CHESSBOARD object containg the data on which you need to estimate priors.
         background
            Path to the TSV file for estimating background priors.
         downsample
            Number of samples in the TSV to use. This is down sampling is to help reduce run time.
     """
    data.am = np.zeros((data.num_lsvs,2))
    data.bm = np.zeros((data.num_lsvs,2))
    # Set up target data regression mat
    mdepth = np.nanmedian(np.sum(data.reads,axis=0),axis=1)
    missrate = (np.sum(np.isnan(np.sum(data.reads,axis=0)),axis=1) + 1) / (data.reads.shape[2] + 1)
    d2 = pd.DataFrame(np.column_stack((missrate,mdepth)),columns=["miss","mdepth"])

    # Set up background regression mat.
    d = pd.read_csv(background)
    d = d[d['miss'] < 0.8]
    d = d.sample(n=downsample, replace='False')
    warnings.filterwarnings("ignore")

    with np.errstate(invalid='ignore'):
        m = Beta.from_formula('miss ~ mdepth', d)
        data.am[:,1], data.bm[:,1] = m.fit().predict(exog=sm.tools.add_constant(mdepth),transform=False)

        m2 = Beta.from_formula('miss ~ mdepth', d2)
        data.am[:,0], data.bm[:,0] = m2.fit().predict()


import numpy as np
import pandas as pd
import statsmodels.api as sm

from scipy.special import gammaln as lgamma

from statsmodels.base.model import GenericLikelihoodModel
from statsmodels.genmod.families import Binomial


# this is only need while #2024 is open.
class Logit(sm.families.links.Logit):
    """Logit tranform that won't overflow with large numbers."""

    def inverse(self, z):
        return 1 / (1. + np.exp(-z))





class Beta(GenericLikelihoodModel):
    """Beta Regression.
    This implementation uses `phi` as a precision parameter equal to
    `a + b` from the Beta parameters.
    """

    def __init__(self, endog, exog, Z=None, link=Logit(),
                 link_phi=sm.families.links.Log(), **kwds):
        """
        Parameters
        ----------
        endog : array-like
            1d array of endogenous values (i.e. responses, outcomes,
            dependent variables, or 'Y' values).
        exog : array-like
            2d array of exogeneous values (i.e. covariates, predictors,
            independent variables, regressors, or 'X' values). A nobs x k
            array where `nobs` is the number of observations and `k` is
            the number of regressors. An intercept is not included by
            default and should be added by the user. See
            `statsmodels.tools.add_constant`.
        Z : array-like
            2d array of variables for the precision phi.
        link : link
            Any link in sm.families.links for `exog`
        link_phi : link
            Any link in sm.families.links for `Z`
        Examples
        --------
        {example}
        See Also
        --------
        :ref:`links`
        """
        assert np.all((0 < endog) & (endog < 1))
        if Z is None:
            extra_names = ['phi']
            Z = np.ones((len(endog), 1), dtype='f')
        else:
            extra_names = ['precision-%s' % zc for zc in \
                           (Z.columns if hasattr(Z, 'columns') else range(1, Z.shape[1] + 1))]
        kwds['extra_params_names'] = extra_names

        super(Beta, self).__init__(endog, exog, **kwds)
        self.link = link
        self.link_phi = link_phi

        self.Z = Z
        assert len(self.Z) == len(self.endog)

    def nloglikeobs(self, params):
        """
        Negative log-likelihood.
        Parameters
        ----------
        params : np.ndarray
            Parameter estimates
        """
        return -self._ll_br(self.endog, self.exog, self.Z, params)

    def fit(self, start_params=None, maxiter=100000, disp=False,
            method='bfgs', **kwds):
        """
        Fit the model.
        Parameters
        ----------
        start_params : array-like
            A vector of starting values for the regression
            coefficients.  If None, a default is chosen.
        maxiter : integer
            The maximum number of iterations
        disp : bool
            Show convergence stats.
        method : str
            The optimization method to use.
        """

        if start_params is None:
            start_params = sm.GLM(self.endog, self.exog, family=Binomial()
                                  ).fit(disp=False).params
            start_params = np.append(start_params, [0.5] * self.Z.shape[1])

        return super(Beta, self).fit(start_params=start_params,
                                     maxiter=maxiter,
                                     method=method, disp=disp, **kwds)

    def _ll_br(self, y, X, Z, params):
        nz = self.Z.shape[1]

        Xparams = params[:-nz]
        Zparams = params[-nz:]

        mu = self.link.inverse(np.dot(X, Xparams))
        phi = self.link_phi.inverse(np.dot(Z, Zparams))
        # TODO: derive a and b and constrain to > 0?

        if np.any(phi <= np.finfo(float).eps): return np.array(-np.inf)

        ll = lgamma(phi) - lgamma(mu * phi) - lgamma((1 - mu) * phi) \
             + (mu * phi - 1) * np.log(y) + (((1 - mu) * phi) - 1) \
             * np.log(1 - y)

        return ll

    def predict(self, params, exog=None):
        if exog is None:
            exog = self.exog

        nz = self.Z.shape[1]
        Xparams = params[:-nz]
        Zparams = params[-nz:]

        mu = self.link.inverse(np.dot(exog, Xparams))
        phi = self.link_phi.inverse(params[-1:])

        return mu * phi, (1-mu) * phi


if __name__ == "__main__":

    EstimateMissingPriors(None,"../data/gtex_missingness.csv")
