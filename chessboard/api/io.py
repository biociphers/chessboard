import numpy as np
import pandas as pd
import pickle
import os
import re

class Data():
    """\
         CHESSBOARD object for storing and manipulating data.

         Stores splicing data and allows user to generate summary statistics and perform processing operations
         on the data.

         Parameters
         ------
         reads
             A 2 x lsv x sample matrix containing junction spanning read outs. On the first axis, index 0 represents
             the number of junction spanning reads and index 1 represents the number of other reads in the normalization
             unit (e.g. LSV, intron cluster, entire gene, etc).
         samples
            Vector of sample names.
         lsvs
            Vector of LSV IDs.
         psi
            A lsv x sample matrix with PSI quantifications.
     """
    def __init__(self,reads:np.ndarray,samples:np.ndarray,lsvs:np.ndarray,psi:np.ndarray):
        self.reads=reads
        self.samples=samples
        self.lsvs=lsvs
        self.psi=psi
        self.am = None
        self.bm = None
        self.r = None
        self.c = None
        self.prob_pw = None
        self.prog_fg = None
        self.a_post = None
        self.b_post = None
        self.am_post = None
        self.bm_post = None
        self.med_tiles = None
        self.pbsfilter = None

    @property
    def num_samples(self):
        """
        Number of samples
        """
        return len(self.samples)

    @property
    def num_lsvs(self):
        """
        Number of LSVs
        """
        return len(self.lsvs)

    def writeChessboardInput(self,filename:str):
        """
        Save data for input to CHESSBOARD algorithm.

        Parameters
        ----------
        filename
            Path to save file.
        """
        np.savez_compressed(filename+".cb",samples=self.samples,
                                     lsv=self.lsvs,
                                     x=self.reads,
                                     psi=self.psi,
                                     am=self.am,
                                     bm=self.bm)
        os.rename(filename + ".cb.npz",filename+".cb")

    def addChessboardOutput(self,filename:str):
        """
        Load results of CHESSBOARD algorithm into object.

        Parameters
        ----------
        filename
            Path to CHESSBOARD algorithm output file.
        """
        result = np.load(filename)
        self.prob_pw = result["prob_pw"]
        self.prob_fg = result["prob_fg"]
        self.a_post = result["a"]
        self.b_post = result["b"]
        self.med_tiles = int(result["med_tiles"])
        if "a_post_chain" in result:
	        self.a_post_chain = result["a_post_chain"]
        if "b_post_chain" in result:
                self.b_post_chain = result["b_post_chain"]

    def writeGambitFile(self, filename:str):
        """
        Save data for input to GAMBIT visualization tool.

        The tool can be found at <https://paros.pmacs.upenn.edu/gambit/>

        Parameters
        ----------
        filename
            Path to save file.
        """
        np.savez_compressed(filename + ".gambit", samples=self.samples,
                            lsv=self.lsvs,
                            reads=self.reads,
                            r=self.r,
                            c=self.c,
                            psi=self.psi,
                            a=self.a_post,
                            b=self.b_post,
                            mp=np.sum(self.r, axis=1).astype(float) / len(self.c),
                            am=self.am_post,
                            bm=self.am_post)
        os.rename(filename + ".gambit.npz",filename+".gambit")


    def subsetSamples(self, index:np.ndarray):
        """
        Retain only samples at the specified indicies.

        Parameters
        ----------
        index
            Numeric or boolean indicies of samples to retain.
        """
        self.reads = self.reads[:, :, index]
        self.samples = self.samples[index]
        self.psi = self.psi[:, index]
        if self.c is not None:
            self.c = self.c[index]
            self.r = self.r[:, index]
            self.prob_fg = self.prob_fg[:, index]
            self.prob_pw = self.prob_pw[:, index]
            self.prob_pw = self.prob_pw[index, :]

    def subsetLSVs(self, index):
        """
        Retain only LSVs at the specified indicies.

        Parameters
        ----------
        index
            Numeric or boolean indicies of LSVs to retain.
        """
        self.reads = self.reads[:, index, :]
        self.lsvs = self.lsvs[index]
        self.psi = self.psi[index, :]
        if self.r is not None:
            self.r = self.r[index, :]
            self.prob_fg = self.prob_fg[index, :]
            self.am = self.am[index, :]
            self.bm = self.bm[index, :]
            self.a_post = self.a_post[index, :]
            self.b_post = self.b_post[index, :]
            self.am_post = self.am_post[index, :]
            self.bm_post = self.bm_post[index, :]


def loadData(file:str):
    """\
        Load MAJIQ generated data.

        Loads MAJIQ generated data processed by :func:`chessboard.api.io.createFromMajiq` in ``.npz`` format as a CHESSBOARD
        data object.

        Parameters
        ------
        file
            Path to the ``.npz`` file.

        Returns
        -------
        :class:`chessboard.api.io.Data`
            A CHESSBOARD object containg the input data.
    """
    data = np.load(file,allow_pickle=True)
    reads = data["reads"]
    psi = data["psi"]
    try:
        samples = data["samples"]
        lsvs = data["lsv"]
    except:
        samples = None
        lsvs = None
    return (Data(reads,samples,lsvs,psi))

def createFromMajiq(dir:str,outfilename:str,quant:int=10,miss:float=0.8):
    """\
        Process and save MAJIQ generated data.

        Extracts junction spanning read rates from MAJIQ [Vaquero-Garcia16]_ files and saves them in ``.npz`` format for
        downstream analysis. The ``.npz`` file is saved in `outfilename`.

        Parameters
        ------
        dir
            Path to the ``.majiq`` files generated using the MAJIQ software.
        outfilename
            Path to the output ``.npz`` file.
        quant
            Minimum number of reads for junction to be quantifiable.
        miss
            Maximum fraction of samples for which a LSV is allowed to be missing before being removed.
    """
    DIR = dir

    files = [x for x in os.listdir(DIR) if x.endswith(".majiq")]

    # Setup output matrix
    tmp = np.load(DIR + files[0], allow_pickle=True)
    N_SAMP = len(files)
    MAX_J = 20
    junc = np.array([x[0] for x in tmp['junc_info']])
    counts = np.unique(junc, return_counts=True)
    keep = counts[0][np.where(counts[1] <= MAX_J)[0]]
    N_LSV = len(keep)
    uni_id = np.array([":".join([j[0].decode('UTF-8'), str(j[1]), str(j[2])]) for j in tmp["junc_info"]])
    order = np.argsort(uni_id)
    keep_idx = np.where(np.isin(junc[order], keep))[0]
    out = np.empty((MAX_J, N_LSV, N_SAMP))
    out[:, :, :] = 0
    debug = np.empty((N_LSV, N_SAMP), dtype='S100')
    debug[:, :] = " "
    for i, f in enumerate(files):
        with np.load(DIR + f) as dat:
            junctions = np.array([x[0] for x in dat["junc_info"]])
            uni_id = np.array([":".join([j[0].decode('UTF-8'), str(j[1]), str(j[2])]) for j in dat["junc_info"]])
            order = np.argsort(uni_id)
            junctions = junctions[order][keep_idx]
            reads = np.median(dat["coverage"], axis=1)[order][keep_idx]
            out[0, 0, i] = reads[0]

            t = 1
            lsv = 0
            debug[0, i] = junctions[0]
            for j in range(1, len(reads)):
                if junctions[j] == junctions[j - 1]:
                    out[t, lsv, i] = reads[j]
                    t += 1
                else:
                    out[t:MAX_J, lsv, i] = np.nan
                    if np.nansum(out[:, lsv, i]) == 0:
                        out[:, lsv, i] = np.nan  # Set missing values
                    t = 0
                    lsv += 1
                    out[t, lsv, i] = reads[j]
                    t += 1
                    debug[lsv, i] = junctions[j]
            out[t:MAX_J, lsv, i] = np.nan
            if np.nansum(out[:, lsv, i]) == 0:
                out[:, lsv, i] = np.nan  # Set missing values

    sample_names = [re.sub(".majiq", "", x) for x in files]

    reads = out
    lsvs = debug[:,0]
    samples = sample_names

    read_cutoff = quant
    quant_row, quant_col = np.where(~np.any(reads > read_cutoff, axis=0))
    reads[:, quant_row, quant_col] = np.nan

    miss_cutoff = miss
    missing_rate = np.sum(np.isnan(reads[0, :, :]), axis=1) / float(reads.shape[2])
    keep = np.where(missing_rate < miss_cutoff)[0]
    reads = reads[:, keep, :]
    lsvs = lsvs[keep]

    # if sys.argv[4] is not None:
    #       sig_lsv = pd.read_csv(sys.argv[4],header=None)
    #       keep = np.in1d(lsvs,sig_lsv[0])
    #       reads = reads[:,keep,:]
    #       lsvs = lsvs[keep]
    #       print(len(lsvs))

    var = np.nanvar(reads / np.nansum(reads, axis=0), axis=2)
    maxvaridx = np.nanargmax(var, axis=0)
    #var = var[maxvaridx, np.arange(reads.shape[1])]
    repj_reads = reads[maxvaridx, np.arange(reads.shape[1]), :]
    reads = np.stack((repj_reads, np.nansum(reads, axis=0) - repj_reads), axis=0)
    psi = (reads[0, :, :] + 0.5) / (np.nansum(reads, axis=0) + 1)
    np.savez_compressed(outfilename,reads=reads,samples=samples,psi=psi,lsv=lsvs)


def loadTSV(file1:str,file2:str):
    """\
        Load data from ``.tsv`` files.

        Load junction spanning read counts produced by other software in ``.tsv`` format. Each column of the ``.tsv`` is
        a sample and each row is a splice junction. ``file1`` should contain the reads mapped to the junction and
        ``file2`` should contain all other reads in the normalization unit (e.g. LSV, intron cluster, entire gene etc).
        Missing values should agree in position between both files.

        Parameters
        ------
        file1
            Path to ``.tsv`` file containing junction spanning read counts.
        file2
            Path to ``.tsv`` containing reads mapped to all other junctions in the splicing normalization unit.

        Returns
        -------
        :class:`chessboard.api.io.Data`
            A CHESSBOARD object containing the input data.
    """
    data = pd.read_csv(file1,sep="\t",index_col=0)
    data2 = pd.read_csv(file2,sep="\t",index_col=0)
    # Check for nan agreement
    if np.sum(np.isnan(data.values) != np.isnan(data2.values)) > 0:
        raise Exception('Missing values in files do no agree. Check nans.')
    # maqji model for psi
    out = np.stack([data.values, data2.values],axis=0)
    return Data(reads=out,samples=data.columns,lsvs=data.index,psi=(data.values+0.5)/(data.values+data2.values+1))


def saveData(data:Data,file:str):
    """\
        Save the current state of a CHESSBOARD object.

        Save the current state of a CHESSBOARD object using pickle which can be loaded using :func: chessboard.api.io.loadCBObject

        Parameters
        ------
        data
            CHESSBOARD object to be saved.
        file
            Path to output file.
    """
    pickle.dump(data, open(file, "wb"))

def loadCBObject(file:str):
    """\
        Load saved CHESSBOARD object.

        Load a pickled CHESSBOARD object.

        Parameters
        ------
        file
            Path to saved object crated using :func: chessboard.api.io.saveData.
    """
    return pickle.load(open(file, "rb"))



