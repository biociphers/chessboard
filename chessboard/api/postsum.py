from .io import Data
import numpy as np
from sklearn.cluster import AgglomerativeClustering
from .io import Data
from scipy.stats import betabinom
from scipy.special import kv, gammaln
from statsmodels.tsa.ar_model import AutoReg, ar_select_order


def generatePointEstimate(data:Data,minprob,force_k=None):
    """Generate a clustering point estimate from MCMC samples.

    When a single tile configuration is of interest, the MCMC samples from CHESSBOARD need to be summarized into a
    point estimate. This method clusters the pairwise sample clustering probability matrix (i.e. the probability that
    any 2 samples are clustered together across MCMC samples) using heirarchical clustering where k is the median
    number clusters across sampled tile configurations to obtain sample assignments. LSV assignments are obtained by
    averaging the marginal probabilities of each matrix entry being in the signal group across all samples in a cluster
    for the given LSV. Posteriors of the distributional parameters are updated based on sample and LSV assignments.

    Parameters
    ----------
    data
        Chessboard object. Must contain CHESSBOARD output after using :func:`chessboard.api.io.Data.addChessboardOutput`.
    minprob
        The minimum probability of the average margin to be considered signal.

     """
    n = data.reads.shape[2]
    m = data.reads.shape[1]
    # Summarize c
    if force_k is None:
        data.c = (AgglomerativeClustering(n_clusters=data.med_tiles).fit(data.prob_pw).labels_).astype(int)
    else:
        data.c = (AgglomerativeClustering(n_clusters=force_k).fit(data.prob_pw).labels_).astype(int)

    # Summarize r
    r = np.zeros((m,n))
    for i in np.unique(data.c):
        r[np.ix_(np.mean(data.prob_fg[:,data.c == i],axis=1) > minprob, data.c == i)] = 1
    data.r = r

    # Get am and bm
    data.am_post = np.column_stack((np.sum(np.isnan(data.psi) * data.r, axis=1), np.sum(np.isnan(data.psi) * (1 - data.r), axis=1))) + data.am
    data.bm_post = np.column_stack((np.sum(~np.isnan(data.psi) * data.r, axis=1), np.sum(~np.isnan(data.psi) * (1 - data.r), axis=1))) + data.bm

    # Model code returns multiple optima
    if len(data.a_post.shape) > 2:
        data.a_post = np.mean(data.a_post, axis=0)
        data.b_post = np.mean(data.b_post, axis=0)

    # Cluster swapping
    # Minority class is signal
    for i, x in enumerate(np.sum(data.r, axis=1) > data.r.shape[1] / 2):
        if x:
            data.r[i, :] = 1 - data.r[i, :]
            data.a_post[i,:] = np.flip(data.a_post[i,:])
            data.b_post[i,:] = np.flip(data.b_post[i,:])
            data.am[i, :] = np.flip(data.am[i, :])
            data.bm[i, :] = np.flip(data.bm[i, :])
            data.am_post[i,:] = np.flip(data.am_post[i,:])
            data.bm_post[i,:] = np.flip(data.bm_post[i,:])
            # Correct for discrete approximation variance (assume fg conc half bg conc)
            data.a_post[i,0] = data.a_post[i,0] / 2
            data.b_post[i, 0] = data.b_post[i, 0] / 2
            data.a_post[i,1] = data.a_post[i,1] * 2
            data.b_post[i, 1] = data.b_post[i, 1] * 2
            data.prob_fg[i,:] = 1 - data.prob_fg[i,:]

def computePostChain(data:Data):
    np.seterr(divide='ignore')
    err = np.log(10e-5)
    achain = data.a_post_chain
    bchain = data.b_post_chain
    ll_chain = []
    for i in np.arange(achain.shape[0]):
        aslice = achain[i,:,:]
        bslice = bchain[i,:,:]
        total_ll = []
        for j in np.arange(data.reads.shape[2]):
            a = aslice[:, 0] * data.r[:,j] + aslice[:, 1] * (1 - data.r[:,j])
            b = bslice[:, 0] * data.r[:,j] + bslice[:, 1] * (1 - data.r[:,j])
            am = data.am_post[:, 0] * data.r[:,j] + data.am_post[:, 1] * (1 - data.r[:,j])
            bm = data.bm_post[:, 0] * data.r[:,j] + data.bm_post[:, 1] * (1 - data.r[:,j])
            xm = np.isnan(data.reads[0,:,j]).astype(float)
            xm[xm == 0] = np.nan
            xn = np.sum(data.reads[:,:,j], axis=0)
            # mll = np.nansum(np.log(betabinom.pmf(xm, 1, am, bm)), axis=0)
            mll = np.log(betabinom.pmf(xm, 1, am, bm))
            vll = np.log(betabinom.pmf(np.round(data.reads[0, :,j]), xn, a, b))
            vll[np.where(vll == -np.inf)[0]] = err
            ll = np.nansum([mll, vll], axis=0)
            total_ll.append(ll)
        ll_chain.append(np.sum(total_ll))
    data.ll_chain = ll_chain


def computeHeidelDiag(data:Data,step=1,pvalue=0.05):

    iter = int(len(data.ll_chain) / 2)
    startvec = np.arange(0,iter+1,step)
    max_order = 10
    sel = ar_select_order(data.ll_chain[(iter+1):], max_order)
    res = sel.model.fit()
    S0 = res.sigma2 / (1 - np.sum(res.params[1:]))**2 # Yule-Walker spectral density
    for i in np.arange(len(startvec)):
        # Compute Cramer-von-Mises test stat
        x = data.ll_chain[startvec[i]:]
        n = len(x)
        b = np.cumsum(x) - np.sum(x) * (np.arange(n) + 1) / n
        bsq = (b * b) / (n * S0)
        integral = np.sum(bsq)/n
        # Compute p-value of test stat
        p = _cdf_cvm_inf(integral)
        if p < 1-pvalue:
            return True, startvec[i] ,1 - p

    return False, None, None



def _cdf_cvm_inf(x):
    """
    Calculate the cdf of the Cramér-von Mises statistic (infinite sample size).
    See equation 1.2 in Csorgo, S. and Faraway, J. (1996).
    Implementation based on MAPLE code of Julian Faraway and R code of the
    function pCvM in the package goftest (v1.1.1), permission granted
    by Adrian Baddeley. Main difference in the implementation: the code
    here keeps adding terms of the series until the terms are small enough.
    The function is not expected to be accurate for large values of x, say
    x > 4, when the cdf is very close to 1.
    """
    x = np.asarray(x)

    def term(x, k):
        # this expression can be found in [2], second line of (1.3)
        u = np.exp(gammaln(k + 0.5) - gammaln(k+1)) / (np.pi**1.5 * np.sqrt(x))
        y = 4*k + 1
        q = y**2 / (16*x)
        b = kv(0.25, q)
        return u * np.sqrt(y) * np.exp(-q) * b

    tot = np.zeros_like(x, dtype='float')
    cond = np.ones_like(x, dtype='bool')
    k = 0
    while np.any(cond):
        z = term(x[cond], k)
        tot[cond] = tot[cond] + z
        cond[cond] = np.abs(z) >= 1e-7
        k += 1

    return tot
