import time
import numpy as np

def progressBar(iterable, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█', printEnd = "\r"):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
        printEnd    - Optional  : end character (e.g. "\r", "\r\n") (Str)
    """
    total = len(iterable)
    # Progress Bar Printing Function
    def printProgressBar (iteration):
        percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
        filledLength = int(length * iteration // total)
        bar = fill * filledLength + '-' * (length - filledLength)
        print(f'\r{prefix} |{bar}| {percent}% {suffix}', end = printEnd)
    # Initial Call
    printProgressBar(0)
    # Update Progress Bar
    for i, item in enumerate(iterable):
        yield item
        printProgressBar(i + 1)
    # Print New Line on Complete
    print()


def RRScore(col_labs, row_labs, col_labs_ref, row_labs_ref):
    total = 0
    sum_p = 0
    sum_r = 0
    for i in np.unique(col_labs).astype(int):
        c = col_labs == i
        for j in np.unique(row_labs[:, i]).astype(int):
            r = row_labs[:, i] == j
            total += 1
            max_p = 0
            max_r = 0

            for k in np.unique(col_labs_ref).astype(int):
                cr = col_labs_ref == k
                for l in np.unique(row_labs_ref[:, k]).astype(int):
                    rr = row_labs_ref[:, k] == l

                    # Precision
                    p = (np.sum(c & cr) + np.sum(r & rr)) / (np.sum(c) + np.sum(r))
                    if p > max_p:
                        max_p = p

                    # Recall
                    rc = (np.sum(c & cr) + np.sum(r & rr)) / (np.sum(cr) + np.sum(rr))
                    if rc > max_r:
                        max_r = rc
            sum_p += max_p
            sum_r += max_r

    return (sum_p / total, sum_r / total)


