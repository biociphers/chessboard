import argparse
from os.path import join


def get_args( __version__ ):
    """Return parsed arguments"""

    parser = argparse.ArgumentParser()

    ### Optional arguments ###

    parser.add_argument("-A", "--conc", type=float, default=1e-100,
        help="Concentration parameter. Larger values increase the number of learned \
        clusters. (default: 1e-100)")

    parser.add_argument("-L", "--reg", type=int, default=50,
        help="Regularization parameter. (default: 50)")

    parser.add_argument("-M", "--minsize", type=int, default=20,
        help="Minimum tile size. (default: 20)")

    parser.add_argument("-I", "--iter", type=int, default=1000,
        help="Number of iterations to run the Gibbs sampler. (default: 1000)")

    parser.add_argument("-B", "--burnin", type=int, default=200,
        help="Number of burn-in iterations. (default: 200)")

    parser.add_argument("-T", "--thin", type=int, default=2,
        help="Number of samples to discard between iterations to reduce autocorrelation. \
        (default: 2)")

    parser.add_argument("-S", "--blocksize", type=int, default=10,
        help="Maximum block size for blocked Gibbs sampling procedure. (default: 10)")

    parser.add_argument("-K", "--kinit", type=int, default=5,
                        help="Number of clusters for k-means initialization. (default: 5)")

    parser.add_argument("-V", "--verbose", default=False, action="store_true",
                        help="Verbose mode. (default: False)")

    parser.add_argument("-MCAR", "--mcar_model", default=False, action="store_true",
                        help="Use MCAR missing value model. (default: False)")


    ### Positional arguments ###

    parser.add_argument("input", type=str,
        help="Path to the chessboard input file.")

    parser.add_argument("out_dir", type=str,
        help="Path to the output directory; will be created if it does not already exist.")

    ### Version ###

    parser.add_argument("--version", action="version", version="%(prog)s "+__version__)

    return parser.parse_args()

def get_consolidate_arguments_and_vars(args):
    """Return a dictionary

    Stores the arguments and some other variables in a dictionary.
    This dictionary is effectively the global namespace for this module,
    which is helpful for multiprocessing.
    """

    d = dict()

    ### Optional arguments

    d["conc"] = args.conc

    d["lambda"] = args.reg

    d["iter"] = args.iter

    d["minsize"] = args.minsize

    d["burnin"] = args.burnin

    d["thin"] = args.thin

    d["blocksize"] = args.blocksize

    d["kinit"] = args.kinit

    d["mcar"] = args.mcar_model

    d["verbose"] = args.verbose

    ### Positional arguments

    d["input"] = args.input

    d["out_dir"] = args.out_dir

    ### Additional files and directories

    d["logfile"] = join(d["out_dir"], "chessboard.log")

    return d