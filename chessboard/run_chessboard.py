import time
import numpy as np
from os.path import join
from os.path import basename
from os import rename
from datetime import datetime,timedelta
from chessboard.globals import d, logfile
from chessboard.model import Model
#from globals import d, logfile
#from model import Model
from . import __version__
#from __init__ import __version__

def write_vars_to_logfile():

    K = list( d.keys() )

    for k in K:
        logfile.write("%s: %s\n"%(str(k), str(d[k])) )

    logfile.flush()

    return None

def main():
    """main method. The program flow is described in additional
    detail at the top of this file.
    """
    start_time = datetime.now()
    tic = time.time()

    logfile.write("Start time: %s\n"%( start_time ))
    logfile.write("CHESSBOARD v. "+__version__+"\n")

    write_vars_to_logfile()

    ## Load data
    logfile.write("Loading Input: %s\n" % datetime.now())
    logfile.flush()
    dat = np.load(d["input"],allow_pickle=True)

    x = dat["x"]
    psi = dat["psi"]
    #a = dat["a"]
    #b = dat["b"]
    #mp = dat["mp"]
    am = dat["am"]
    bm = dat["bm"]

    logfile.write("Creating Model Object: %s\n" % datetime.now())
    logfile.flush()

    m = Model(x, psi, am, bm, d["conc"], d["lambda"], d["kinit"], d["blocksize"], d["minsize"], d["mcar"], d["verbose"])
    #m = Model(x, None,None,None,None,None, d["conc"], d["lambda"], d["blocksize"], d["minsize"])

    logfile.write("Starting Inference: %s\n" % datetime.now())
    logfile.flush(),

    a_posterior, b_posterior, post_samples_pw_mean, post_samples_fg_mean, med_tiles, a_post_chain, b_post_chain  = m.inference(d["iter"], d["burnin"], d["thin"])
    print(med_tiles)
    logfile.write("Inference completed. Writing output: %s\n" % datetime.now())
    logfile.flush()


    np.savez_compressed(join(d["out_dir"],basename(d["input"])[:-3] + "_out.cb"),a=a_posterior,b=b_posterior,
                        prob_pw=post_samples_pw_mean, prob_fg=post_samples_fg_mean, med_tiles=med_tiles, a_post_chain=a_post_chain, b_post_chain=b_post_chain)

    rename(join(d["out_dir"],basename(d["input"])[:-3] + "_out.cb.npz"), join(d["out_dir"],basename(d["input"])[:-3] + "_out.cb"))

    toc = time.time()
    logfile.write("Chessboard run successful!")
    logfile.write("Total runtime (HH:MM:SS): %s" % timedelta(seconds=toc - tic))
    logfile.flush()


if __name__ == '__main__':
    main()
