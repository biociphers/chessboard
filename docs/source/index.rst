Welcome to CHESSBOARD's documentation!
======================================

**CHESSBOARD** is a python implementation of the algorithm described in *Wang et al. 2022*. It also comes with
an API to facilitate preprocessing of raw data and exploration of algorithm output.

Check out the :doc:`usage` section for further information, including
how to install the project.

To see an example workflow, check out the Jupyter Notebook at :doc:`CHESSBOARD_Basic_Usage`.

To visualize data, check out the `GAMBIT tool <https://paros.pmacs.upenn.edu/gambit/>`_

.. note::

   This project is under active development.

Contents
--------

.. toctree::

   usage
   api
   CHESSBOARD_Basic_Usage
   references

