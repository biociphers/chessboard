Usage
=====

Installation
------------

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install CHESSBOARD and the API.

.. code-block:: console

   $ pip install git+https://bitbucket.org/biociphers/chessboard



Usage
-----

CHESSBOARD is a command line tool that can be run as follows using the provided arguments. Note that the input is a
CHESSBOARD ``.cb`` file which can be generated using our API. The API allows the user to import raw data for processing
prior to running CHESSBOARD and explore the algorithm output for downstream analsysis.

.. code-block:: console


    usage: chessboard [-h] [-A CONC] [-L REG] [-I ITER] [-B BURNIN] [-T THIN]
                         [-S BLOCKSIZE] [-M minsize] [-K kinit] [-V verbose] [--version]
                         input out_dir

    positional arguments:
      input                 Path to the chessboard input file.
      out_dir               Path to the output directory; will be created if it
                            does not already exist.

    optional arguments:
      -h, --help            show this help message and exit
      -A CONC, --conc CONC  Concentration parameter. Larger values increase the
                            number of learned clusters. (default: 1e-100)
      -L REG, --reg REG     Regularization parameter. (default: 50)
      -I ITER, --iter ITER  Number of iterations to run the Gibbs sampler.
                            (default: 1000)
      -B BURNIN, --burnin BURNIN
                            Number of burn-in iterations. (default: 200)
      -T THIN, --thin THIN  Number of samples to discard between iterations to
                            reduce autocorrelation. (default: 2)
      -S BLOCKSIZE, --blocksize BLOCKSIZE
                            Maximum block size for blocked Gibbs sampling
                            procedure. (default: 10)
      -M MINSIZE, --minsize MINSIZE
                Minimum tile size. (default: 20)

      -K KINIT, --kinit KINIT
                Number of clusters for k-means initialization. (default: 5)

      -V, --verbose
                Verbose mode. (default: False)
      --version             show program's version number and exit










