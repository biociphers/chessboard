import os
import sys
from pathlib import Path
__location__ =  Path(__file__).parent
sys.path.insert(0, os.path.join(__location__, "../.."))

# Configuration file for the Sphinx documentation builder.

# -- Project information

project = 'CHESSBOARD'
copyright = '2022, David Wang'
author = 'David Wang'

release = '0.1'
version = '0.1.0'

# -- General configuration

extensions = [
    'sphinx.ext.duration',
    'sphinx.ext.doctest',
    'sphinx.ext.autodoc',
    'sphinx.ext.autosummary',
    'sphinx.ext.intersphinx',
    'sphinx.ext.napoleon',
    'sphinx.ext.mathjax',
    'sphinx_autodoc_typehints',
    "nbsphinx"
]

rst_prolog = """
.. |default| raw:: html

    <div class="default-value-section">""" + \
    ' <span class="default-value-label">Default:</span>'

intersphinx_mapping = {
    'python': ('https://docs.python.org/3/', None),
    'sphinx': ('https://www.sphinx-doc.org/en/master/', None),
}
intersphinx_disabled_domains = ['std']

templates_path = ['_templates']
pygments_style = 'sphinx'

# -- Options for HTML output

html_theme = 'scanpydoc'
autosummary_generate = True
napoleon_google_docstring = False
napoleon_numpy_docstring = True
napoleon_include_init_with_doc = False
napoleon_use_rtype = True  # having a separate entry generally helps readability
napoleon_use_param = True
napoleon_custom_sections = [('Params', 'Parameters')]

# -- Options for EPUB output
epub_show_urls = 'footnote'
