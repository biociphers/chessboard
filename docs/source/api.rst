.. module:: chessboard.api
.. automodule:: chessboard.api
   :noindex:

API
===


Import CHESSBOARD as::

   import chessboard.api as cb


Read and Write: ``io``
----------------------

.. module:: chessboard.api.io
.. currentmodule:: chessboard.api

Functions for reading and writing data. Users need to read input data into a CHESSBOARD :py:class:`io.Data`
object to perform all downstream operations, data manipulation and analysis. CHESSBOARD currently accept 2 input data
types.

1. MAJIQ builder ``.majiq`` files. These files contain junction spanning read counts from MAJIQ [Vaquero-Garcia16]_.
2. Equivalent data processed through another junction mapping software in the form of ``.tsv`` files.


.. autosummary::
   :toctree: generated/

   io.Data
   io.createFromMajiq
   io.loadData
   io.loadTSV
   io.saveData
   io.loadCBObject


Data Preprocessing: ``prefilter``
---------------------------------

.. module:: chessboard.api.prefilter
.. currentmodule:: chessboard.api

Functions for prefiltering informative LSVs as described in *Wang et al. 2022*.

.. autosummary::
   :toctree: generated/

   prefilter.VarFilter
   prefilter.PBSFilter

Prior Estimation: ``priors``
----------------------------

.. module:: chessboard.api.priors
.. currentmodule:: chessboard.api

Functions for estimating priors.

.. autosummary::
   :toctree: generated/

   priors.EstimateMissingPriors

Posterior Summary: ``postsum``
------------------------------

.. module:: chessboard.api.postsum
.. currentmodule:: chessboard.api

Functions for summarizing posterior samples.

.. autosummary::
   :toctree: generated/

   postsum.generatePointEstimate


Downstream Analysis: ``analysis``
---------------------------------

.. module:: chessboard.api.analysis
.. currentmodule:: chessboard.api

Functions for summarizing posterior samples.

.. autosummary::
   :toctree: generated/

   analysis.outputLSVLists
   analysis.computeLikelihood

Visualization: ``vis``
----------------------

.. module:: chessboard.api.vis
.. currentmodule:: chessboard.api

Visualization tools.

.. autosummary::
   :toctree: generated/

   vis.PlotECDF
   vis.PlotPointEstimate
   vis.PlotPointEstimate
   vis.Heatmap
   vis.Histogram



