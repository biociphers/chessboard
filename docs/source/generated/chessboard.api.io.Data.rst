﻿chessboard.api.io.Data
======================

.. currentmodule:: chessboard.api.io

.. autoclass:: Data

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~Data.__init__
      ~Data.addChessboardOutput
      ~Data.subsetLSVs
      ~Data.subsetSamples
      ~Data.writeChessboardInput
      ~Data.writeGambitFile
   
   

   
   
   .. rubric:: Attributes

   .. autosummary::
   
      ~Data.num_lsvs
      ~Data.num_samples
   
   