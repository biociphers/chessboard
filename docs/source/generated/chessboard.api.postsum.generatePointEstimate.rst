﻿chessboard.api.postsum.generatePointEstimate
============================================

.. currentmodule:: chessboard.api.postsum

.. autofunction:: generatePointEstimate