﻿chessboard.api.vis.ECDF
=======================

.. currentmodule:: chessboard.api.vis

.. autoclass:: ECDF

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~ECDF.__init__
   
   

   
   
   