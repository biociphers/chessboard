References
----------

.. [Vaquero-Garcia16] Vaquero-Garcia *et al.* 2016,
   *A new view of transcriptome complexity and regulation through the lens of local splicing variations*,
   `eLife <https://elifesciences.org/articles/11752>`__.