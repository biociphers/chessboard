# CHESSBOARD v1.0

Implementation of CHESSBOARD (Characterizing Heterogenity of Expression and Splicing by Searching for Blocks of Outliers and Abnormalities in RNA Datasets) from Wang et al. 2021.
CHESSBOARD is a Bayesian algorithm for identifying tiles in a matrix of splicing data.
Chessboard takes as input a matrix of read counts mapped to splice junctions obtained from MAJIQ in npz format. 
Chessboard outputs posterior samples for indicators that determine whether a subset of LSVs in a subset of samples is considered signal (tile) or background. 
Chessboard is accompanied by a suite of python tools for prefiltering the data, estimating priors, summarizing posterior samples, and visualizing data.
For interactive visualization, the GAMBIT (Graphical Annotated MAP for Basic Inspection of Tiles) will allow users to explore the biological significane of tiles found by Chessboard.


## Installation and updating
Use the package manager [pip](https://pip.pypa.io/en/stable/) to install the CHESSBOARD API.

```bash
pip install git+https://bitbucket.org/biociphers/chessboard
```

The installation can be completed in several seconds.

## Requriements

The CHESSBOARD software has been tested with the following dependencies. 

```
python_version >= "3.8"
pandas >= 1.1.5
scipy >= 1.5.4
numpy >= 1.19.1
seaborn >= 0.11.1
statsmodels >= 0.12.2
scikit-learn >= 0.21.3
matplotlib >= 3.3.4
```

## Arguments

```
usage: chessboard [-h] [-A CONC] [-L REG] [-I ITER] [-B BURNIN] [-T THIN]
                     [-S BLOCKSIZE] [-M minsize] [-K kinit] [-V verbose] [--version]
                     input out_dir

positional arguments:
  input                 Path to the chessboard input file.
  out_dir               Path to the output directory; will be created if it
                        does not already exist.

optional arguments:
  -h, --help            show this help message and exit
  -A CONC, --conc CONC  Concentration parameter. Larger values increase the
                        number of learned clusters. (default: 1e-100)
  -L REG, --reg REG     Regularization parameter. (default: 50)
  -I ITER, --iter ITER  Number of iterations to run the Gibbs sampler.
                        (default: 1000)
  -B BURNIN, --burnin BURNIN
                        Number of burn-in iterations. (default: 200)
  -T THIN, --thin THIN  Number of samples to discard between iterations to
                        reduce autocorrelation. (default: 2)
  -S BLOCKSIZE, --blocksize BLOCKSIZE
                        Maximum block size for blocked Gibbs sampling
                        procedure. (default: 10)
  -M MINSIZE, --minsize MINSIZE
			Minimum tile size. (default: 20)

  -K KINIT, --kinit KINIT
			Number of clusters for k-means initialization. (default: 5)
  
  -V, --verbose
			Verbose mode. (default: False)
  --version             show program's version number and exit	  
```

# CHESSBOARD API

A Python API for processing and analyzing data for the CHESSBOARD algorithm. 
Full documentation for the API can be found in Readthedocs format [here](https://chessboard.readthedocs.io/en/latest/index.html)

## Usage

See vignettes/tutorial for basic usage examples. The tutorial can be run and completed in around 10 minutes. To try the demo, simply clone this repo and  open the Jupyter
Notebook in the vignettes folder. The output can be viewed on the API documentation page as well.


